#!/usr/bin/env sh
# Check status of the clamd server

RET=$(echo PING | nc 0.0.0.0 3310)

[ $? != 0 -o $RET != "PONG" ] && exit 1

exit 0
