# ClamAV Daemon Docker Image

_Debian_ based image that runs a ClamAV daemon.

Features:
 - build ClamAV from source
 - install `clamd`, `freshclam` and `clamdtop`
 - generate the database in the volume `/var/lib/clamav` with freshclam
 - the entrypoint populate the database if  the volume `/var/lib/clamav` is empty
 - `/var/lib/clamav` populated with ClamAV database during build
 - _Supervisor_ is used to launch `clamd` and `freshclam`
 - `clamd` compiled with Prelude support

## Quick Start

### Build and Run

```
docker build -t clamd .
docker run  -p 3310:3310 -v $(pwd)/data:/var/lib/clamav --name clamd clamd
```

### Using the registry image and docker-compose

```
docker-compose up -d
```

## References

 1. [ClamAV](https://www.clamav.net/)
 2. [Supervisor](http://supervisord.org/)
 3. [Prelude OSS](https://www.prelude-siem.org/)
